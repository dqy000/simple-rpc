package com.dqy.anno;

import com.dqy.configuration.ClientRunner;
import com.dqy.warp.ClientRunnerWarp;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Documented
@Target({ElementType.TYPE,ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)  // 保留到运行时，可通过注解获取
@Import(ClientRunner.class)
public @interface EnableSimpleRpcClient {

}
