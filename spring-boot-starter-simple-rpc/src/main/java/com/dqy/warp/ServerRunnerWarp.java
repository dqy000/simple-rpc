package com.dqy.warp;

import com.dqy.properties.SimpleRpcProperties;
import org.springframework.beans.factory.annotation.Autowired;
import server.CoreServer;
import support.RpcContext;

import javax.annotation.Resource;

public class ServerRunnerWarp {
    @Resource
    SimpleRpcProperties simpleRpcProperties;

    public void runRpcServer(){
        CoreServer instance = CoreServer.getInstance(simpleRpcProperties.getPort());
        RpcContext.initHostAndPort(simpleRpcProperties.getCenterAddr(),simpleRpcProperties.getCenterPort());
        RpcContext.initRpcInfo(simpleRpcProperties.getScanPack());
        RpcContext.startSpyThread(simpleRpcProperties.getReconnectionTime());
    }
}
