package com.dqy.warp;

import com.dqy.properties.SimpleRpcProperties;
import factory.ProxyFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;

public class ClientRunnerWarp {
    @Resource
    SimpleRpcProperties simpleRpcProperties;

    public void initClient(){
        try {
            ProxyFactory.initHostAndPort(simpleRpcProperties.getCenterAddr(),simpleRpcProperties.getCenterPort());
            ProxyFactory.init(simpleRpcProperties.getScanPack());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
