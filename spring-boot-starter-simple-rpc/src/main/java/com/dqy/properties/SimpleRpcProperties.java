package com.dqy.properties;


import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "simple-rpc")
public class SimpleRpcProperties {
    private String scanPack; //扫描@config,也可以不适用@Config注解来连接，直接配置centerAddr，centerPort也行

    private Integer port = 28888; //服务端启动端口

    private Boolean enabledReconnection =false; //是否开启自动重连

    private Integer reconnectionTime=3000; // 重连间隔

    private Boolean enabledSpringSupport =false; //启用Spring 容器支持

    private String centerAddr; //连接注册中心的地址

    private Integer centerPort; //注册中心端口

    public String getCenterAddr() {
        return centerAddr;
    }

    public void setCenterAddr(String centerAddr) {
        this.centerAddr = centerAddr;
    }

    public Integer getCenterPort() {
        return centerPort;
    }

    public void setCenterPort(Integer centerPort) {
        this.centerPort = centerPort;
    }



    public Boolean getEnabledSpringSupport() {
        return enabledSpringSupport;
    }

    public void setEnabledSpringSupport(Boolean enabledSpringSupport) {
        this.enabledSpringSupport = enabledSpringSupport;
    }

    public Integer getReconnectionTime() {
        return reconnectionTime;
    }

    public void setReconnectionTime(Integer reconnectionTime) {
        this.reconnectionTime = reconnectionTime;
    }

    public Boolean getEnabledReconnection() {
        return enabledReconnection;
    }

    public void setEnabledReconnection(Boolean enabledReconnection) {
        this.enabledReconnection = enabledReconnection;
    }

    public String getScanPack() {
        return scanPack;
    }


    public void setScanPack(String scanPack) {
        this.scanPack = scanPack;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }
}
