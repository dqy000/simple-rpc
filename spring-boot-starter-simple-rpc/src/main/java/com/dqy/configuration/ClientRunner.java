package com.dqy.configuration;

import com.dqy.warp.ClientRunnerWarp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ImportAware;
import org.springframework.core.type.AnnotationMetadata;

public class ClientRunner implements ImportAware {
    @Autowired
    ClientRunnerWarp clientRunnerWarp;

    @Override
    public void setImportMetadata(AnnotationMetadata importMetadata) {
        if(clientRunnerWarp !=null){
            clientRunnerWarp.initClient();
        }
    }
}
