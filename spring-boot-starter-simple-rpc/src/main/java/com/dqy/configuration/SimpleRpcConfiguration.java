package com.dqy.configuration;

import com.dqy.properties.SimpleRpcProperties;
import com.dqy.warp.ClientRunnerWarp;
import com.dqy.warp.ServerRunnerWarp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import support.Constans;
import support.ObjectPoolSupport;

@Configuration
@EnableConfigurationProperties(value = SimpleRpcProperties.class)
//@ConditionalOnClass(SimpleRpcProperties.class)
public class SimpleRpcConfiguration {
    @Autowired
    SimpleRpcProperties simpleRpcProperties;


    @Bean(name = Constans.OBJECT_BEAN_NAME)
    @ConditionalOnClass(ObjectPoolSupport.class)
    public ObjectPoolSupport objectPoolSupport(){
        return new ObjectPoolSupport();
    }

    @Bean
    @ConditionalOnMissingBean(ServerRunnerWarp.class)
    public ServerRunnerWarp serverRunnerWarp(){
        return new ServerRunnerWarp();
    }

    @Bean
    @ConditionalOnMissingBean(ClientRunnerWarp.class)
    public ClientRunnerWarp clientRunnerWarp(){
        return new ClientRunnerWarp();
    }
}
