package com.dqy.configuration;

import com.dqy.warp.ServerRunnerWarp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ImportAware;
import org.springframework.core.type.AnnotationMetadata;

public class ServerRunner implements ImportAware {
    @Autowired
    ServerRunnerWarp serverRunnerWarp;

    @Override
    public void setImportMetadata(AnnotationMetadata importMetadata) {
        if(serverRunnerWarp!=null){
            serverRunnerWarp.runRpcServer();
        }
    }
}
