package codec;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import support.RpcInfo;
import util.SerializeTool;

import java.util.List;


//解码器
public class DataDecode extends ReplayingDecoder<Void> {
    static Logger logger = LoggerFactory.getLogger(DataDecode.class);

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        logger.info("开始解码...");

        long len = in.readLong();
        byte[] bytes = new byte[(int) len];
        ByteBuf byteBuf = in.readBytes(bytes);

        RpcInfo info = SerializeTool.byte2Object(bytes, RpcInfo.class);

        out.add(info);
        logger.info("解码完毕,得到的内容:{}",info);
    }
}
