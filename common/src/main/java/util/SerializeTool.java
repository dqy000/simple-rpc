package util;

import support.RpcInfo;

import java.io.*;


//序列化工具
public class SerializeTool {
    static public byte[] object2Byte(Object o) throws Exception {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream outputStream = new ObjectOutputStream(byteArrayOutputStream);
        outputStream.writeObject(o);
        return byteArrayOutputStream.toByteArray();
    }

    static public <T>  T byte2Object(byte[] bytes,Class<T> tClass) throws Exception{
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        ObjectInputStream inputStream =new ObjectInputStream(byteArrayInputStream);
        T object = (T) inputStream.readObject();
        return object;
    }

    public static void main(String[] args) throws Exception {
        RpcInfo rpcInfo = new RpcInfo();
        rpcInfo.setArgs(new String[]{"1", "2"});
        rpcInfo.setClassName("ddd.aaa");

        byte[] bytes = object2Byte(rpcInfo);
        System.out.println(bytes.length);

        RpcInfo rpcInfo1 = byte2Object(bytes, RpcInfo.class);
        System.out.println(rpcInfo1);
    }
}
