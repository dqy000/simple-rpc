package util;

import java.util.List;

public class StringUtil {
    static public boolean strIsEmpty(String s){
        return null==s || "".equals(s);
    }

    static public boolean listIsEmpty(List list){
        return null==list || list.size()==0;
    }

    static public String getSimpleName(String className){
        String[] split = className.split("\\.");
        String simpleName = split[split.length - 1];

        return simpleName;
    }

    public static int TIME_OUT = 10;
}
