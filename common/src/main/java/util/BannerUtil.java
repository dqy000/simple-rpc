package util;

import java.io.*;
import java.net.URL;

public class BannerUtil {
    static public void showBanner(Class c){
        try {
            InputStream resourceAsStream = c.getClassLoader().getResourceAsStream("simple-rpc-banner.txt");
            assert resourceAsStream!=null;

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resourceAsStream));
            String line=null;
            while ((line = bufferedReader.readLine())!=null)
                System.out.println(line);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
