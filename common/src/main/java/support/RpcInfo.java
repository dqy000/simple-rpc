package support;

import java.io.Serializable;
import java.util.Arrays;

public class RpcInfo implements Serializable ,Cloneable{
    private String className;
    private String methodName;

    private Object[] args;
    private Object callRes;

    private Class[] methodType;


    private String errorCode;

    ///////与注册中心交互字段////////
    private int command; //0:注册 ，1:调用
    private int port;
    private String host;

    private int serverToCenterPort; //Rpc服务端与注册中心通信的端口

    ////////////////////////


    public RpcInfo(String className, String methodName, Class[] methodType) {
        this.className = className;
        this.methodName = methodName;
        this.methodType = methodType;
    }

    public RpcInfo(){}

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return "RpcInfo{" +
                "className='" + className + '\'' +
                ", methodName='" + methodName + '\'' +
                ", args=" + Arrays.toString(args) +
                ", callRes=" + callRes +
                ", methodType=" + Arrays.toString(methodType) +
                ", errorCode='" + errorCode + '\'' +
                ", command=" + command +
                ", port=" + port +
                ", host='" + host + '\'' +
                ", serverToCenterPort=" + serverToCenterPort +
                '}';
    }

    public int getServerToCenterPort() {
        return serverToCenterPort;
    }

    public void setServerToCenterPort(int serverToCenterPort) {
        this.serverToCenterPort = serverToCenterPort;
    }

    public int getCommand() {
        return command;
    }

    public void setCommand(int command) {
        this.command = command;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public Class[] getMethodType() {
        return methodType;
    }

    public void setMethodType(Class[] methodType) {
        this.methodType = methodType;
    }

    public Object getCallRes() {
        return callRes;
    }

    public void setCallRes(Object callRes) {
        this.callRes = callRes;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Object[] getArgs() {
        return args;
    }

    public void setArgs(Object[] args) {
        this.args = args;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
