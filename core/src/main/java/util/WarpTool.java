package util;

import support.RpcInfo;
import support.anno.RemoteFunction;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class WarpTool {


    static boolean isNotEmpty(List list){
        return list!=null && list.size()!=0;
    }

    static public RpcInfo warpInfo(Class dclass,Method method,Object[] args){
        RpcInfo res = new RpcInfo();
        res.setMethodName(method.getName());
        res.setArgs(args);
        res.setClassName(dclass.getName());
        res.setMethodType(method.getParameterTypes());
        return res;
    }

    static public List<RpcInfo> classWrapToInfo(Class clazz){
        List<RpcInfo> res = new ArrayList<>();
        Method[] declaredMethods = clazz.getDeclaredMethods();

        if(declaredMethods != null){
            for(Method method : declaredMethods){
                RemoteFunction annotation = method.getAnnotation(RemoteFunction.class);
                if(annotation!=null){
                    //add
                    RpcInfo info = new RpcInfo();
                    info.setClassName(clazz.getName()); //验证是不是com.xx.xxx.ClassName的形式
                    info.setMethodName(method.getName());
                    info.setMethodType(method.getParameterTypes());

                    res.add(info);
                }
            }
        }

        return res;
    }

    static public void mergeList(List local,List all){
        if(isNotEmpty(local) && isNotEmpty(all)){
            for (int i = 0; i <local.size() ; i++) {
                all.add(local.get(i));
            }
        }
    }

}
