package support;

import support.event.SimpleRpcEvent;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;

public class EventQueue {

    private BlockingQueue<SimpleRpcEvent> queue = new ArrayBlockingQueue<SimpleRpcEvent>(5);

    public void put(SimpleRpcEvent event){
        try {
            queue.put(event);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public SimpleRpcEvent pop(){
        try {
            return queue.take();
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }
}
