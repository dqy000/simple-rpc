package support.anno;

import java.lang.annotation.*;

@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)  // 保留到运行时，可通过注解获取
public @interface Caller {
    String host() default "";
    int port() default -1;

    // true:一所有调用共享一个连接，性能较低，资源消耗较少
    // false: 不共享，一个接口建立一个连接
    boolean share() default false;

    //使用注册中心，
    // false：点对点调用
    // true: 从注册中心调用,此时忽略host,port
    boolean toCenter() default false;
}
