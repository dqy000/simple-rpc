package support.anno;

import java.lang.annotation.*;


//表明这是一个远程方法，作用于方法上

@Documented
@Target(ElementType.METHOD)  //  注解用于方法上
@Retention(RetentionPolicy.RUNTIME)  // 保留到运行时，可通过注解获取
public @interface RemoteFunction {
}
