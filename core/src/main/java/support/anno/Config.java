package support.anno;

import java.lang.annotation.*;

//配置类注解，在一个应用程式中请确保只能出现一次
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)  // 保留到运行时，可通过注解获取
public @interface Config {
    String centerHostName() default "";
    int centerPort() default -1;
}
