package support.anno;


//表明这是一个可被远程调用的类,修饰于类上面

import javax.annotation.Resource;
import java.lang.annotation.*;

@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)  // 保留到运行时，可通过注解获取
public @interface RemoteClass {

}
