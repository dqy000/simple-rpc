package support.event;

import java.util.EventObject;

public class SimpleRpcEvent extends EventObject {

    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public SimpleRpcEvent(Object source) {

        super(source);
    }


}
