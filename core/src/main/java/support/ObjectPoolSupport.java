package support;

import ch.qos.logback.core.spi.ContextAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import server.handle.DataInboundHandle;

import javax.swing.*;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ObjectPoolSupport implements ApplicationContextAware {

    static ApplicationContext context;

    static public boolean springSupport = false;

    public static ApplicationContext getContext() {
        return context;
    }

    static Logger logger = LoggerFactory.getLogger(ObjectPoolSupport.class);

    //对象池，在不整合spring的情况下会使用，整合spring忽略这个
    private static Map<String , Object> objectPool = new ConcurrentHashMap<>();

    public Object getObject(String key){
        logger.info("更具 {} 查找对象池",key);
        return objectPool.get(key);
    }

    public void putObject(String key,Object o){
        logger.info("添加新的实列进入对象池");
        objectPool.put(key,o);
    }

    public Object getObjectForContex(String benName){
        logger.info("从spring容器取{} bean",benName);
        Object bean = context.getBean(benName);
        return bean;
    }
    public <T> T getObjectForClassContex(Class<T> tClass){
        logger.info("从spring容器取{} bean",tClass);
        T bean = context.getBean(tClass);
        return bean;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
        springSupport=true;
    }
}
