package error;

public class RemoteMethodNotFoundException extends Exception{

    public RemoteMethodNotFoundException() {
    }

    public RemoteMethodNotFoundException(String message) {
        super(message);
    }

    public RemoteMethodNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public RemoteMethodNotFoundException(Throwable cause) {
        super(cause);
    }

    public RemoteMethodNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
