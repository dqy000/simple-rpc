package error;

public class InitRpcContextExeption extends Exception{
    public InitRpcContextExeption() {
    }

    public InitRpcContextExeption(String message) {
        super(message);
    }

    public InitRpcContextExeption(String message, Throwable cause) {
        super(message, cause);
    }

    public InitRpcContextExeption(Throwable cause) {
        super(cause);
    }

    public InitRpcContextExeption(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
