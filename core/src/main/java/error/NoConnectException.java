package error;

public class NoConnectException extends Exception{
    public NoConnectException() {
    }

    public NoConnectException(String message) {
        super(message);
    }

    public NoConnectException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoConnectException(Throwable cause) {
        super(cause);
    }

    public NoConnectException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
