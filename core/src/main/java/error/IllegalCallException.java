package error;

public class IllegalCallException extends Exception{
    public IllegalCallException() {
    }

    public IllegalCallException(String message) {
        super(message);
    }

    public IllegalCallException(String message, Throwable cause) {
        super(message, cause);
    }

    public IllegalCallException(Throwable cause) {
        super(cause);
    }

    public IllegalCallException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
