package server.handle;

import codec.DataDecode;
import codec.DataEncode;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;

public class InitHandle extends ChannelInitializer<SocketChannel> {

    protected void initChannel(SocketChannel socketChannel) throws Exception {
        socketChannel.pipeline().addLast(new DataDecode());
        socketChannel.pipeline().addLast(new DataEncode());

        socketChannel.pipeline().addLast(new DataInboundHandle());
    }
}
