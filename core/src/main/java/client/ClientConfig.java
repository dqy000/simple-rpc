package client;

public class ClientConfig {
    String centerHost;
    int centerPort;

    public ClientConfig(String centerHost, int centerPort) {
        this.centerHost = centerHost;
        this.centerPort = centerPort;
    }

    public String getCenterHost() {
        return centerHost;
    }

    public void setCenterHost(String centerHost) {
        this.centerHost = centerHost;
    }

    public int getCenterPort() {
        return centerPort;
    }

    public void setCenterPort(int centerPort) {
        this.centerPort = centerPort;
    }
}
