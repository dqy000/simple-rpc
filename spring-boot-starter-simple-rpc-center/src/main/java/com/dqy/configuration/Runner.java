package com.dqy.configuration;

import com.dqy.properties.SimpleRpcCenterProperties;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.ImportAware;
import org.springframework.context.annotation.ImportSelector;
import org.springframework.core.type.AnnotationMetadata;
import server.CenterServer;
import server.route.IRoute;

public class Runner implements ImportAware, ApplicationContextAware {

    static private ApplicationContext context;

    @Autowired
    SimpleRpcCenterProperties simpleRpcCenterProperties;

    @Override
    public void setImportMetadata(AnnotationMetadata importMetadata) {
        CenterServer bean = context.getBean(CenterServer.class);
        String lbStrategy = simpleRpcCenterProperties.getLbStrategy();

        Object lbObject = context.getBean(lbStrategy);
        CenterServer.iRoute = (IRoute) lbObject;

        bean.run();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }
}
