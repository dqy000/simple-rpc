package com.dqy.configuration;

import com.dqy.properties.SimpleRpcCenterProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.*;
import server.CenterServer;
import server.route.impl.PollingRoute;
import server.route.impl.RandomRoute;

@Configuration
@EnableConfigurationProperties(value = SimpleRpcCenterProperties.class)
public class SimpleRpcCenterConfiguration {
    @Autowired
    SimpleRpcCenterProperties simpleRpcCenterProperties;

    @ConditionalOnClass(CenterServer.class)
    @Bean
    public CenterServer centerServer(){
        Integer port = simpleRpcCenterProperties.getPort();

        Integer workerThreadPoolSize = simpleRpcCenterProperties.getWorkerThreadPoolSize();

        CenterServer centerServer = new CenterServer(port,workerThreadPoolSize);

        return centerServer;
    }

    @ConditionalOnMissingBean(RandomRoute.class)
    @Bean(name = "random")
    public RandomRoute randomRoute(){
        return new RandomRoute();
    }

    @ConditionalOnMissingBean(PollingRoute.class)
    @Bean(name = "polling")
    public PollingRoute pollingRoute(){
        return new PollingRoute();
    }

}
