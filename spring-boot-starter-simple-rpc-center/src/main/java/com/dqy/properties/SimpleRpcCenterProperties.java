package com.dqy.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "simple-rpc-center")
public class SimpleRpcCenterProperties {
    Integer port =33333; //注册中心启动端口
    Integer workerThreadPoolSize=5; //工作线程池大小
    String lbStrategy = "random"; //负载策略

    public Integer getWorkerThreadPoolSize() {
        return workerThreadPoolSize;
    }

    public void setWorkerThreadPoolSize(Integer workerThreadPoolSize) {
        this.workerThreadPoolSize = workerThreadPoolSize;
    }

    public String getLbStrategy() {
        return lbStrategy;
    }

    public void setLbStrategy(String lbStrategy) {
        this.lbStrategy = lbStrategy;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }
}
