package server.support;

import server.error.RepeatRegisteException;
import server.error.ServerNotFoundException;
import server.route.IRoute;
import support.RpcInfo;

public interface IServerInfoMap {
    void registe(RpcInfo info) throws RepeatRegisteException;
    void unregiste(RpcInfo info);
    void unregiste(String host,int port);
    RpcInfo findServer(RpcInfo info, IRoute route) throws ServerNotFoundException;
}
