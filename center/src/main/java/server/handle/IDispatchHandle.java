package server.handle;

import io.netty.channel.socket.nio.NioSocketChannel;
import support.RpcInfo;

public interface IDispatchHandle {
    void dispatch(RpcInfo info, NioSocketChannel channel);
}
