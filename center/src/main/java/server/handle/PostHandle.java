package server.handle;

import io.netty.channel.socket.nio.NioSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import server.CenterServer;
import server.route.IRoute;
import server.route.impl.PollingRoute;
import server.route.impl.RandomRoute;
import support.RpcInfo;

public class PostHandle implements IDispatchHandle {
    static Logger logger = LoggerFactory.getLogger(PostHandle.class);
    NioSocketChannel channel;

    public NioSocketChannel getChannel() {
        return channel;
    }

    public void setChannel(NioSocketChannel channel) {
        this.channel = channel;
    }

    IRoute route = CenterServer.iRoute;

    @Override
    public void dispatch(RpcInfo info,NioSocketChannel channel) {
        logger.info("POST-HANDLE-BEGINE...");
        try {
            RpcInfo server = ServerInboundHandle.serverInfoMap.findServer(info,route);
            server.setErrorCode(ServerInboundHandle.REGIST_OK);
            channel.writeAndFlush(server);
            logger.info("POST-HANDLE-END...");
        }catch (Exception e){
            info.setErrorCode(e.getMessage());
            e.printStackTrace();
            channel.writeAndFlush(info);
            logger.info("POST-HANDLE-FAIL...");
        }
    }
}
