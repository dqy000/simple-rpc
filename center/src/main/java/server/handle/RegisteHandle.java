package server.handle;

import io.netty.channel.socket.nio.NioSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import support.RpcInfo;

public class RegisteHandle implements IDispatchHandle{
    static Logger logger = LoggerFactory.getLogger(RegisteHandle.class);
    NioSocketChannel channel;

    public NioSocketChannel getChannel() {
        return channel;
    }

    public void setChannel(NioSocketChannel channel) {
        this.channel = channel;
    }

    @Override
    public void dispatch(RpcInfo info,NioSocketChannel channel) {
        logger.info("REGISTE-HANDLE-BEGINE...");
        try {
            ServerInboundHandle.serverInfoMap.registe(info);
            info.setErrorCode(ServerInboundHandle.REGIST_OK);
            channel.writeAndFlush(info);
            logger.info("REGISTE-HANDLE-END...");
        }catch (Exception e){
            info.setErrorCode(e.getMessage());
            channel.writeAndFlush(info);
            logger.info("REGISTE-HANDLE-FAIL...");
        }
    }
}
