package server.error;

public class RepeatRegisteException extends Exception{
    public RepeatRegisteException() {
    }

    public RepeatRegisteException(String message) {
        super(message);
    }

    public RepeatRegisteException(String message, Throwable cause) {
        super(message, cause);
    }

    public RepeatRegisteException(Throwable cause) {
        super(cause);
    }

    public RepeatRegisteException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
