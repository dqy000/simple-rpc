package server.route.impl;

import server.route.IRoute;
import support.RpcInfo;

import java.util.List;
import java.util.Random;

//随机选择
public class RandomRoute implements IRoute {
    @Override
    public RpcInfo selectServer(List<RpcInfo> infos) {
        Random random =new Random();
        return infos.get(random.nextInt(infos.size()));
    }

}
