package server.route.impl;

import server.route.IRoute;
import support.RpcInfo;
import util.StringUtil;

import java.util.List;

public class PollingRoute implements IRoute {
    @Override
    public RpcInfo selectServer(List<RpcInfo> infos) {
        RpcInfo res = infos.get(0);

        String key = genKey(res);
        if(!addAsEmpty(key)) { //首次调用这个服务才会新增
            Integer nowIndex = inc(key);
            if(nowIndex>= infos.size()){ //越界，重置下标
                updateIndex(key,0);
            }else {
                return infos.get(nowIndex);
            }
        }
        return res;
    }
}
