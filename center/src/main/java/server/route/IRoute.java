package server.route;

import support.RpcInfo;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public interface IRoute {
    Map<String, AtomicInteger> routeMap = new ConcurrentHashMap<>();

    default boolean addAsEmpty(String key){
        if(routeMap.get(key)==null) {
            routeMap.put(key, new AtomicInteger(0));
            return true;
        }
        return false;
    }

    default Integer inc(String key){
        AtomicInteger atomicInteger = routeMap.get(key);
        return atomicInteger.incrementAndGet();
    }

    default void updateIndex(String key,int val){
        AtomicInteger atomicInteger = routeMap.get(key);
        assert atomicInteger !=null;
        atomicInteger.set(val);
    }

    default String genKey(RpcInfo info){
        String className = info.getClassName();
        String methodName = info.getMethodName();
        Class[] methodType = info.getMethodType();
        StringBuilder builder = new StringBuilder();
        builder.append(className);
        builder.append("@");
        builder.append(methodName);
        builder.append("[");
        if(methodType!=null && methodType.length>0){
            for (Class c: methodType){
                builder.append(c.getSimpleName() + ",");
            }
        }
        builder.append("]");
        return builder.toString();
    }

    RpcInfo selectServer(List<RpcInfo> infos);
}
